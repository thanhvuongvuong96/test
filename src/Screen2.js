import React,{Component} from 'react';
import {Alert,StyleSheet,View,Text,SafeAreaView, Image,TextInput,Button,TouchableOpacity  } from 'react-native';

import 'react-navigation';
export default class Screen2 extends Component {


    constructor(props) {
        super(props);
        this.state = {
            number1:"",
            number2:"",
            result:""
        }
    }
    componentWillMount(){
        const { navigation } = this.props;
        const number1 = navigation.getParam("number1","number1")
        const number2 = navigation.getParam("number2","number2")

        this.setState({number1})
        this.setState({number2})
    }

    _onPresstru =  () =>{
        const number1 = parseFloat(this.state.number1)
        const number2 = parseFloat(this.state.number2)

        let res = number1 - number2
        let result = number1.toString() + "-" + number2.toString() + " = " +res.toString()
        this.setState({result}, () => {
            this.props.navigation.navigate('Screen1',{result: this.state.result})
        })
       
    }

    _onPresschia =  () =>{
        const number1 = parseFloat(this.state.number1)
        const number2 = parseFloat(this.state.number2)

        let res = number1 / number2
        let result = number1.toString() + "/" + number2.toString() + " = " +res.toString()
        this.setState({result}, () => {
            this.props.navigation.navigate('Screen1',{result: this.state.result})
        })
    }

    render() {
        
        return(
            <SafeAreaView style={styles.container}>
                <View style={styles.logobg}>
                    <Text style={styles.logotext}>CALCULATOR</Text>
                </View>    
                <View style={styles.inputform}>
                    <View style={styles.inputrow}>
                        <Text style={styles.inputt}> Number 1:</Text>
                        <TextInput style={styles.inputi}
                        value={this.state.number1}
                        >

                        </TextInput>
                    </View>

                    <View style={styles.inputrow}>
                        <Text style={styles.inputt}> Number 2:</Text>
                        <TextInput style={styles.inputi}
                        value={this.state.number2}
                        >

                        </TextInput>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:1}}></View>
                        
                        <TouchableOpacity
                        onPress={this._onPresstru}
                        style={styles.btn}
                        >
                        <Text style={styles.text}>-</Text>
                        </TouchableOpacity>
                        
                        <View style={{flex:0.4}}></View>
                        
                        <TouchableOpacity
                        onPress={this._onPresschia}
                        style={styles.btn}
                        >
                        <Text style={styles.text}>/</Text>
                        </TouchableOpacity>

                        <View style={{flex:1}}></View>
                    </View>
                    

                </View>
                <View style={styles.resultform}>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
    },
    logobg:{
        marginTop:5,
        backgroundColor:'rgb(59, 89, 152)',
        flex:1,
        width:360,
        alignItems: 'center',
    },
    logotext:{
        marginTop:4,
        fontSize:30,
        fontWeight:'bold'
    },
    inputform:{
        width:360,
        flex:4,
    },
    inputrow:{
        flex:2,
        flexDirection:'row',
        alignItems: 'center',
    },
    inputt:{
        flex:1,
        
    },
    inputi:{
        flex:3,
        textDecorationLine:'underline',
        textDecorationColor:'black'
    },
    inputr:{
        textDecorationLine:'underline',
        textDecorationColor:'black'
    },
    btn:{
        flex:1.5,
        backgroundColor:'grey',
        alignItems:'center',
        borderRadius:2,
    },
    resultform:{
        flex:7,
        flexDirection:'column',
        alignItems: 'center',
    },
    text:{
        marginTop:5,
    }
})