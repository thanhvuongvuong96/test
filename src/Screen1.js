import React,{Component} from 'react';
import {Alert,StyleSheet,View,Text,SafeAreaView, Image,TextInput,Button,TouchableOpacity, FlatList  } from 'react-native';

export default class Screen1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number1:"",
            number2:"",
            result:[]
        }
    }
    componentWillReceiveProps(nextprops) {
        const { navigation } = nextprops;
        const result = navigation.getParam("result")
        this.setState({result: [...this.state.result, result]})
    }
    _onPressButton =  () =>{
        this.props.navigation.navigate('Screen2',{number1: this.state.number1, number2: this.state.number2})
    }

    onTextChanged1(text) {
        // code to remove non-numeric characters from text
        this.setState({number1: text})

      }

      onTextChanged2(text) {
        // code to remove non-numeric characters from text
        this.setState({number2: text})
      }
    render() {
        
        return(
            <View style={styles.container}>
                <View style={styles.logobg}>
                    <Text style={styles.logotext}>CALCULATOR</Text>
                </View>    
                <View style={styles.inputform}>
                    <View style={styles.inputrow}>
                        <Text style={styles.inputt}> Number 1:</Text>
                        <TextInput style={styles.inputi} 
                        keyboardType='numeric'
                        onChangeText={(text)=> this.onTextChanged1(text)}
                        value={this.state.number1}
                        >

                        </TextInput>
                    </View>

                    <View style={styles.inputrow}>
                        <Text style={styles.inputt}> Number 2:</Text>
                        <TextInput style={styles.inputi} 
                        keyboardType='numeric'
                        onChangeText={(text)=> this.onTextChanged2(text)}
                        value={this.state.number2}
                        >
                        </TextInput>
                    </View>

                    <TouchableOpacity
                    onPress= {this._onPressButton}
                    style={styles.btnsend}
                    >
                    <Text style={styles.text}>SEND</Text>
                    </TouchableOpacity>

                </View>
                <View style={styles.resultform}>
                    <FlatList 
                        data={this.state.result}
                        renderItem={({item}) => {
                            return(
                                <View style={{alignItems:'center', borderBottomColor:'black'}}>
                                    <Text>{item}</Text>
                                </View>
                            )
                        }}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
    },
    logobg:{
        marginTop:5,
        backgroundColor:'rgb(59, 89, 152)',
        flex:1,
        width:360,
        alignItems: 'center',
    },
    logotext:{
        marginTop:3,
        fontSize:30,
        fontWeight:'bold'
    },
    inputform:{
        width:360,
        flex:4,
    },
    inputrow:{
        flex:2,
        flexDirection:'row',
        alignItems: 'center',
    },
    inputt:{
        flex:1
    },
    inputi:{
        flex:3,
        textDecorationLine:'underline',
        textDecorationColor:'black'
    },
    inputr:{
        width:360,
        textDecorationLine:'underline',
        textDecorationColor:'black'
    },
    btnsend:{
        backgroundColor:'grey',
        width:100,
        flex:1,
        marginTop:20,
        right:0,
        marginLeft:260,
        alignItems:'center',
    },
    resultform:{
        flex:7,
        flexDirection:'column',
        alignItems: 'center',
    },
    text:{
        marginTop:5,
    }
})