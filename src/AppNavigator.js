import { createStackNavigator } from 'react-navigation';
import Screen1 from './Screen1';
import Screen2 from './Screen2';

const AppNavigator = createStackNavigator({
  Screen1: { 
    screen: Screen1,
    navigationOpions : {
      header: "MiddleTest"
    }
  },
  Screen2:{ screen: Screen2
  }
});

export default AppNavigator;